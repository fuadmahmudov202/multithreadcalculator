package az.ubank;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Main {
    private static Double max=0.0;
    public static void main(String[] args) throws Exception {
//        ArrayList<String> list = new ArrayList<>();
//        ArrayList<String> secondList = new ArrayList<>();
//
//        ScriptEngineManager scm = new ScriptEngineManager();
//        ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
//        Double max = 0.0;
//        Double min = 0.0;
//        try (BufferedReader br = new BufferedReader(new FileReader("question.txt"))) {
//            String line;
//            while ((line = br.readLine()) != null) {
//                if (line.contains("!"))
//                    secondList.add(line);
//                else
//                    list.add(line);
//            }
//        } catch (Exception e) {
//
//        }
        readFileSingleThread("question.txt");
//        readMultipleThread(list,secondList);

    }

    private static void readMultipleThread(ArrayList<String> list, ArrayList<String> secondList) throws InterruptedException {
        System.out.println(list.size());
        Runnable runnable= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=0;i<list.size()/5;i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        Runnable runnable2= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=list.size()/5;i<list.size()/5*2;i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread2 = new Thread(runnable2);
        thread2.start();
        Runnable runnable3= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=list.size()/5*2;i<list.size()/5*3;i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread3 = new Thread(runnable3);
        thread3.start();
        Runnable runnable4= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=list.size()/5*3;i<list.size()/5*4;i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread4 = new Thread(runnable4);
        thread4.start();
        Runnable runnable5= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=list.size()/5*4;i<list.size();i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread5 = new Thread(runnable5);
        thread5.start();
        thread2.join();

        Runnable runnable6= () -> {
            ScriptEngineManager scm = new ScriptEngineManager();
            ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
            for (int i=list.size()/5*4;i<list.size();i++) {
                evaluate(i,list,jsEngine);
            }
        };
        Thread thread6 = new Thread(runnable6);
        thread6.start();
        System.out.println(max);
    }

    private static void evaluate(int i, ArrayList<String> list, ScriptEngine jsEngine) {
        Double eval=null;
        Integer integer=null;
        Object eval1 = null;
        try {
            eval1 = jsEngine.eval(list.get(i));
        } catch (ScriptException e) {
            throw new RuntimeException(e);
        }
        if (eval1.getClass().equals(Integer.class))
            integer = (int) eval1;
        else
            eval= (double) eval1;

        if (eval!=null&&eval>max)
            max=eval;
        else if (integer!=null&& integer>max)
            max=integer.doubleValue();
    }


    public static void readFileSingleThread(String path) throws Exception {

        ScriptEngineManager scm = new ScriptEngineManager();
        ScriptEngine jsEngine = scm.getEngineByName("JavaScript");
        Double max = 0.0;
        Double min = 0.0;
        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("line"+line);
            }
        } catch (Exception e) {

        }

    }
}